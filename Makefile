ci:
	docker-compose -f docker-compose.yml run app make setup
	docker-compose -f docker-compose.yml up --abort-on-container-exit

compose-setup: compose-build
	docker-compose run app make setup

compose-build:
	docker-compose build

compose-bash:
	docker-compose run app bash

compose-test:
	docker-compose run app make test

compose-lint:
	docker-compose run app make lint

compose:
	docker-compose up

compose-down:
	docker-compose down -v --remove-orphans

setup:
	composer install
	cp -n code-env code/.env || true

test:
	composer exec -v phpunit -- --colors=always --testdox

lint:
	composer exec -v phpcs code
	composer run-script phpstan

code-start:
	make -C code start
