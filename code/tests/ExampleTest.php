<?php

namespace Tests;

use Laravel\Lumen\Testing\{
    DatabaseMigrations,
    DatabaseTransactions
};

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->get('/');

        $this->assertEquals(
            $this->app->version(),
            $this->response->getContent()
        );
    }
}
